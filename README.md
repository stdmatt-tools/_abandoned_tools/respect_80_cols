# respect-80-cols

**Made with <3 by [stdmatt](http://stdmatt.com).**

#
# I DO NOT SUPPORT THE PROGRAM ANYMORE.
#


## Description:

```respect-80-cols``` is a small utility that helps makes the text files fits
into 80 columns max. It can runs ok in normal text, but fails to do useful things
in source code.

I created it because back then I was with **LOADS** of text that I need to fit 
in 80 columns, but never used after that task. Anyways, this can be useful in the 
future... perhaps...

<br>

As usual, you are **very welcomed** to **share** and **hack** it.


## Usage:
```shell script
Usage:
  respect-80-columns [--help] [--version]
  respect-80-columns [--in-place] <filenames>

  *--help    : Show this screen.
  *--version : Show program version and copyright.

  --in-place: Modify the file in place, otherwise print the contents to stdout.

Notes:
  --in-place option apply to all filenames that are given to the program.

  Options marked with * are exclusive, i.e. the respect-80-columns will run that
  and exit after the operation.
```

## Install:

```shell script
## 1- Clone...
git clone https://gitlab.com/stdmatt-tools/respect_80_cols.git
## 2 - Go to directory...
$ cd ./respect_80_cols
## 3 - Install...
$ sudo ./install.sh 
## 4 - Have fun ;D
```
## License:

This software is released under [GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html).


## Others:

There's more FLOSS things at [stdmatt.com](https://stdmatt.com) :)
